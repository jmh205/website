# Getting Started with the Cloud

## Introduction

!!! warning

    This page covers a rapidly changing topic. Reader beware. This page is
    correct as of {{ git.date.strftime("%B %Y") }}.

This page provides guidance on how to take your first steps using public cloud
products. It will be of interest to the following people within University
Information Services (UIS):

* those developing or designing new services, and
* those co-ordinating service delivery.

Those co-ordinating service delivery may be particularly interested in the
[getting a cloud account](#getting-a-cloud-account) section. Those developing or
designing new services may be particularly interested in the hands on guides and
the description of "what you get" when you request access to a public cloud.

## Getting a cloud account

!!! important

    This section is intended for members of UIS. Procedures relating to cloud
    products are evolving over time. Make sure to check the UIS [hosting
    services](https://help.uis.cam.ac.uk/service/network-services/hosting-services/)
    page for the latest procedures and advice.

There are three steps required to make use of cloud products for a service or
for personal experimentation:

* selecting a public cloud provider,
* getting a cost centre code for billing, and
* having some initial infrastructure provisioned for you.

The following sections describe the steps, as of writing, you will need to
follow if you want to request access to a public cloud. Unfortunately much of
this is still a manual process. UIS are working to make this process more
automated and self-service over time.

### Selecting a cloud

UIS are currently in a position to offer access to two public clouds within UIS:
[Amazon Web Services](https://aws.amazon.com/) (AWS) and [Google
Cloud](https://cloud.google.com). Only AWS is currently offered to the rest of
the University. More public cloud providers are likely to become available over
time.

As of writing, UIS has more in-house expertise with Google Cloud but does make
use of cloud products from AWS as well. Anecdotal evidence from UIS members
suggests that Google Cloud is slightly easier to work with whereas AWS has a
greater range of products and more documentation available in the public domain.

The Technical Design Authority (TDA) does not recommend any particular public
cloud at this time.

### Getting a cost centre code

Once you have selected a public cloud, make use of either the
[Google](https://cloud.google.com/products/calculator) or
[Amazon](https://calculator.aws/) pricing calculator to get an estimate for the
monthly cost of your service. This need not be accurate to the penny but should
provide an order of magnitude indication of expected cost.

For personal experimentation Team Leads should estimate a reasonable cost based
on the products their team wishes to experiment with.

Email this estimate to [finance@uis.cam.ac.uk](mailto:finance@uis.cam.ac.uk) and
request that they allocate a forecast for the expenditure and provide you with a
cost centre code in return.

### Requesting initial infrastructure

Once you have a cost centre code, email
[cloudsupport@uis.cam.ac.uk](mailto:cloudsupport@uis.cam.ac.uk) specifying which
cloud(s) you wish to use, the cost centre code and an initial set of account
administrators. For business continuity reasons it is preferable to have at
least two administrators. The cloud products you use will be grouped together
under a single name. If the resources are intended to be used for a single
service or personal experimentation please make that clear in the email.

What you get in return will depend on the cloud you have requested.

#### Amazon Web Services

AWS Identity and Access Management (IAM) has the concept of a *root user* where
all authority to create, delete and manage cloud products originates. A root
user can create multiple *IAM users* and grant each user access to cloud
products by means of *IAM policies*. This process is described in a [getting
started
document](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started.html)
provided by AWS.

You will be given a [root user
identity](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction_identity-management.html)
which can be used to create IAM users which, in turn, will be able to manage
cloud products.

!!! warning

    As its name suggests the "root user" is a powerful credential and should
    only be used to bootstrap the initial set of IAM users.

#### Google Cloud

!!! important

    UIS are in the process of finalising how Google Cloud will be made available
    to the rest of The University. This provision is subject to change.

There are [three sorts of Google
account](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/automating-google-drive/)
which are used in Google Cloud: ordinary Google accounts, G Suite accounts and
service accounts. The majority of UIS members will have a `{CRSid}@cam.ac.uk` G
Suite account which they can access using their Raven credentials. Service
accounts are created within the Google Cloud platform and are authenticated by a
private key as opposed to a password.

Google groups cloud resources into
*[projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects)*
and groups projects into
*[folders](https://cloud.google.com/resource-manager/docs/creating-managing-folders)*.

When Google Cloud is provisioned for you, you will get:

* a Google folder for admins to create projects in,
* a *meta project* within the folder which contains cloud resources used to
    administer the Google Cloud service within UIS,
* a service account within the meta project which has the same rights as admins,
    and
* a private key which can be used to authenticate as the service account stored
    as a [Secret Manager](https://cloud.google.com/secret-manager) secret within
    the meta project.

Additionally, admins will be given rights to [associate projects with the UIS
billing account](https://cloud.google.com/billing/docs/how-to/modify-project)
but only within their Google folder.

!!! warning

    The G Suite account of a product admin is a high-value credential. It is
    mandatory that admins protect their account with [two-step
    verification](https://www.google.com/landing/2step/).

## Hands on with Amazon Web Services

!!! info

    This section has not yet been written. Contributions are welcome via merge
    requests in the [Developers' Hub
    project](https://gitlab.developers.cam.ac.uk/uis/tda/website/) for this
    website.

## Hands on with Google

This site has a [dedicated page](./hands-on-google-cloud.md) which describes a
simple deployment using Google Cloud.

## External resources

* [UIS documentation on buying AWS
    services](https://help.uis.cam.ac.uk/service/network-services/hosting-services/AWS).
* [Google Cloud Platform Essential
    Training](https://www.linkedin.com/learning/google-cloud-platform-essential-training-3?u=2963594):
    LinkedIn Learning course.
* [Introduction to AWS for
    Non-Engineers](https://www.linkedin.com/learning/introduction-to-aws-for-non-engineers-1-cloud-concepts-2?u=2963594):
    LinkedIn Learning course.
* [Learning AWS for
    Developers](https://www.linkedin.com/learning/learning-amazon-web-services-aws-for-developers-2?u=2963594):
    LinkedIn Learning course.
