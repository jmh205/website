# Coding standards

When we talk about code, we refer to any type of code, regardless
of language (including markup), or purpose (including infrastructure as code 
or [configuration management](/standards/deployment)).

## Revision control

The TDA requires that any code developed, maintained, or managed by UIS:

1. The code must be held in a revision control system.
2. git is to be the type of revision control system.
3. The [University's Developer Hub](https://gitlab.developers.cam.ac.uk/)
should be used as the specific code repository.
4. The code should be licensed "as open as possible" and contributions 
should be accepted from the community.
