# Website style

This page documents the style of writing used on the Technical Design Authority
(TDA) website and is intended to ensure that the TDA speak with a single voice
and tone. It is not intended to be *proscriptive* in how one must write in
general; it is intended to be *descriptive* of the TDA's voice on this site.

## Audiences

The TDA site should be a useful resource for the majority of UIS members. These
members will have different roles within the organisation and so pages should
always be written assuming that it will be read by multiple audiences.

Do not use "you" unless the audience has previously been defined. For example,
"when you are deploying services..." would not be appropriate in a section aimed
at all audiences.

Conversely, make free use of "you" when writing directly to a single audience.

### Procurement

The TDA site should be a useful resource for those involved in the selection
and evaluation of third-party products. This can encompass multiple roles and,
for the sake of brevity we call this the "procurement audience".

Writing for a procurement audience should set any TDA standards in the context
of specific requirements which we expect products to meet.

The procurement audience is not necessarily technical. They *do not* necessarily
need a great deal of justification or rationale for a standard. They *do* need
clear requirements which can be placed into procurement documents. They also
benefit from guidance around determining whether a particular vendor response
indicates that a requirement is met.

When writing for a procurement audience:

* Include examples of requirements in a form useful for inserting in tender
    documents, requests for information (RFIs) or requests for proposals (RFPs).
* Include examples of responses which meet requirements.
* Provide contact points for technical experts within University Information
    Services (UIS) who can assist with the drafting and evaluation of vendor
    responses.
* Make it clear if a requirement will also impose on-going or up-front work for
    UIS. For example, if a requirement implies an integration with an existing
    system, indicate whether UIS would be expected to perform that integration
    and to maintain it.

### Engineering

The TDA site should be a useful resource for those involved in the technical
delivery of a service. This can encompass multiple roles and, for the sake of
brevity we call this the "engineering audience".

Writing for an engineering audience should set any TDA standards alongside
information on how they may be implemented and why they are as they are.

The engineering audience is, by virtue of their role, technical in nature. As
people used to making minute-to-minute technical decisions when implementing
services, they prefer to be well informed and for standards to have clear
rationale and justification behind them.

When writing for an engineering audience:

* Include a rationale and justification for standards. Do not "appeal to
    authority" when writing these as it can appear as "[cargo
    culting](https://en.wikipedia.org/wiki/Cargo_cult)". Instead, provide
    a reasoned discussion of the standard along with examples of how it helps
    UIS as a whole.
* Provide links to material providing overviews of related technology, "getting
    started" guides and, if possible, examples of the standard being implemented
    elsewhere in UIS.
* Reduce the barrier to experimentation as much as possible. A technology which
    requires elaborate approval or on-boarding processes to evaluate will be
    adopted less than a technology which can be experimented with in a few
    minutes.
* Be precise in your language. Avoid "[weasel
    wording](https://en.wikipedia.org/wiki/Weasel_word)" in order to smooth
    any internal divides. The engineering audience spends their working lives
    looking for ambiguity or inconsistency and will keenly spot it in writing.

### Co-ordinators

The TDA site should be a useful resource for those co-ordinating the delivery of
a service. This can encompass multiple roles spanning project, line and sprint
management. For the sake of brevity we call this the "co-ordinator audience".

Writing for a co-ordinator audience should set any TDA standards in the context
of their relative priority. The co-ordinator audience may be juggling multiple
streams of work and they will need to determine when work is required to bring a
service in line with TDA standards and which standards are the most important to
implement.

When writing for a co-ordinator audience:

* Make it clear whose responsibility it is to implement and police a standard.
    In particular indicate if it involves on-going engineering work.
* Be explicit when a standard is dependent on another standard. For
    example, an authentication standard which mandates OAuth2 implicitly
    requires that the service be delivered in part over the web.
* Allow a non-technical reader to understand what a particular standard aims to
    do and how one may determine if that aim is met.
* Indicate at what point in a service's life-cycle the TDA should be approached.

### External

The TDA site is a public website. Development is done in the open in a [public
Developers' Hub project](https://gitlab.developers.cam.ac.uk/uis/tda/website/).
As such it may be read by those external to UIS. An external audience may have
been directed to the TDA site as an example of practice to be followed or
avoided. The TDA should strive to make sure that the former is more common than
the latter.

## Structure

Pages providing guidance or standards should stand alone when read from
beginning to end. They should have an introduction which sets out what the page
will contain. They may have an abstract to allow the main points to be
emphasised.

Consider each page in the context of the TDA audiences above. If appropriate
include a section on each page indicating who the page is written for. Larger
pages will probably benefit from a section directing particular audiences to
sections of most relevance to them.

When appropriate, provide a collection of external resources at the bottom of a
page for those who wish to dive deeper into the topic.

## Tone

The TDA is intended, as part of its terms of reference, to provide guidance to
the UIS. This site should be written as guidance firstly and instruction
secondarily.

The tone of this site should be that of a colleague mentoring another.  In
particular it should not suggest that TDA guidance is in some way self-evident.
Nor should it pre-suppose that the audience has complete understanding of the
topic covered.

Prefer to be whimsical rather than po-faced, to be educational rather than
merely informative and to be conversational rather than authoritarian.

## Vocabulary

Much of the TDA site will be concerned with setting technical standards and
providing advice. It is important that it is clear when the site contains
direction (i.e. what people *must* do) and when it contains guidance (i.e. what
people *should* do).

An engineering audience may already by familiar with the content of [RFC
2119](https://tools.ietf.org/html/rfc2119) and so it makes sense to adopt its
definitions of the following terms, reproduced here verbatim:

* "**must**": This word, or the terms "**required**" or "**shall**", mean that
   the definition is an absolute requirement of the specification.
* "**must not**": This phrase, or the phrase "**shall not**", mean that the
   definition is an absolute prohibition of the specification.
* "**should**": This word, or the adjective "**recommended**", mean that there
   may exist valid reasons in particular circumstances to ignore a
   particular item, but the full implications must be understood and
   carefully weighed before choosing a different course.
* "**should not**": This phrase, or the phrase "**not recommended**" mean that
   there may exist valid reasons in particular circumstances when the
   particular behaviour is acceptable or even useful, but the full
   implications should be understood and the case carefully weighed
   before implementing any behaviour described with this label.
* "**may**": This word, or the adjective "**optional**", mean that an item is
   truly optional.  One vendor may choose to include the item because a
   particular marketplace requires it or because the vendor feels that
   it enhances the product while another vendor may omit the same item.
   An implementation which does not include a particular option **must** be
   prepared to interoperate with another implementation which does
   include the option, though perhaps with reduced functionality. In the
   same vein an implementation which does include a particular option
   **must** be prepared to interoperate with another implementation which
   does not include the option (except, of course, for the feature the
   option provides.)

To cater for an external audience, try to provide a summary definition of
University of Cambridge-specific terminology when it is introduced. Along with
helping an external reader, this also helps orientate new hires when they read
this site.

When first introducing a technical term, try to link it to a page elsewhere on
the web providing an overview of the term. Wikipedia is a good source of a
consensus view of technical terminology. Remember that someone reading the site
may be a member of that day's [lucky 10,000](https://xkcd.com/1053/).

## Voice

The TDA site should always be written in the [active
voice](https://en.wikipedia.org/wiki/Active_voice) where possible. It should be
written from the perspective of the independent observer and not, for example,
as if it were the TDA speaking directly. It should assume a general audience
unless in a section explicitly directed at a single audience. Some examples of
ways of saying "the TDA recommends the use of Cloud services where possible"
which do not match this style:

* "Cloud services, where possible, are recommended by the TDA" - use of passive
    voice.
* "We recommend the use of Cloud Services where possible" - writing *as* the
    TDA, not *about* it.
* "Cloud services must be used" - unclear about whether this is the TDA or some
    or external requirement imposing the restriction and does not specify *who*
    should be using Cloud Services or when.
* "The TDA recommends that Cloud Services be used when you implement your
    service" - [begging the
    question](https://en.wikipedia.org/wiki/Begging_the_question) of whether the
    audience is implementing a service.

## Formatting

When a technical term is first introduced on a page, format it *in italics*. If
appropriate, make it a hyperlink pointing to a resource where a reader can find
out mode.

When an initialism or acronym is present on a page, spell it out explicitly on
first use and place the initialism or acronym in brackets after.

Hyperlinks should be nouns or phrases, not verbs. [Click here is considered
harmful](https://www.google.com/search?q=click+here+considered+harmful).

If text quotes some UI element such as a button's label or dialogue box's title,
format it *in bold*.

## Nomenclature

The word "service" should if at all possible only be used to refer to
UIS-provided services. If third-party services are being discussed, prefer words
such as "products", "offerings" or "tools" when describing them. This will avoid
confusion between whether UIS or a third-party is responsible for the design and
delivery of a "service".

## Contact details

When providing email contact details prefer role addresses to personal
addresses. Ideally pages on this site should remain correct as people move
around within UIS. Where personal addresses must be used always include the
person's role so that an appropriate contact can be found if the person moves
on.

## External resources

A style guide can never be exhaustive and will almost always be controversial.
Examples of other guides which can be used to answer questions of style include:

* [How to write in plain
    English](https://www.plainenglish.co.uk/how-to-write-in-plain-english.html)
* [BBC News style
    guide](https://www.bbc.co.uk/academy/en/collections/news-style-guide)
* [Microsoft technical documentation style
    guide](https://docs.microsoft.com/en-us/style-guide/welcome/)
* [Google developer documentation style
    guide](https://developers.google.com/style/)
