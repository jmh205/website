# TDA website

This repository contains the source for the UIS [Technology Design 
Authority](https://techdesign.uis.cam.ac.uk/) website.

## Developing locally

The website uses mkdocs to convert Markdown source into a static html 
website.
Install mkdocs using pip:

```console
$ pip3 install --user -r requirements.txt
```

You can then serve a site locally for development via ``mkdocs``:

```console
$ mkdocs serve
```

The documentation is now available at http://localhost:8000/.

## Hosting

The website is hosted as [a
project](https://readthedocs.org/dashboard/uis-technology-design-authority/) on
readthedocs.org (RTD) and is automatically built on each commit to master. RTD
natively supports mkdocs documentation and so, beyond informing RTD that this is
a mkdocs project, no configuration is required to build the docs.

The ``techdesign.uis.cam.ac.uk`` DNS record is a CNAME pointing to
``readthedocs.io`` following the [custom domain
documentation](https://docs.readthedocs.io/en/stable/custom_domains.html) from
RTD.

Currently, the RTD configuration is manual. To gain access to the project, create
a RTD account and ask an existing administrator to add you as a maintainer to
the project.

RTD knows when to build the documentation by means of a
[webhook](https://docs.readthedocs.io/en/stable/webhooks.html). This has to be
manually added to the Developer Hub project as an
[integration](https://gitlab.developers.cam.ac.uk/uis/tda/website/hooks/28/edit).
The Developer Hub project should be configured to POST to the RTD webhook on
push events. [GitLab-specific
documentation](https://docs.readthedocs.io/en/stable/webhooks.html#gitlab) is
provided by RTD.
